# Conchology

## About

Shell script library with common useful functionality used by other scripts.

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2020 privb0x23
